# midterm

midterm project ( zalo interface)

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

## Tutorials to launch project (05/06/2022)
- As this project using SMS services and Phone Authentication of Firebase Authentication:
- The project require the SHA-1 and SHA-256 code of your virtual simulator (or your real mobile devices) to run this project.
- Currently, there are 2 solutions for you to launch due to firebase authentication error
1) Using your own project created via Firebase with unlocked phone services,gmail services
2) Easily giving your SHA-1 and SHA-256 to admin of this project to add this SHA code to the project security. 

##  Notice :
- While login via google email, the application may rise the alert that "Google has verified this app", click advanced then click go to app (unsafe) or you won't get inside the app content (home).
<p >
  <img src="https://i.stack.imgur.com/lAcC1.png" width="550" alt="accessibility text">
</p>

- If using phone number as login method, i recommend you should use Fake phone number to receive sms , here is link for you:
https://receive-smss.com/

- *** If you not receive sms code , high chances that Firebase has block the device due to sending too many sms code at the same time 
( Or due to 50 limit per day of Firebase license for free edition).

## Detail tutorials for Solution 1:
+ Create firebase project and enable phone services at sign-in-method tab of Authentication option of the firebase project.
<p >
  <img src="https://jsmobiledev.com/img/sign-in-method.png" width="350" alt="accessibility text">
</p>
+ Download google-services.json file of your firebase project and add it to folder "app" inside folder "android" of the project. (as you can see my google-services.json with this path above).

+ Get your sha-1 and sha-256 code from your virtual simulator or real mobile devices.

+ Add your sha-1 and sha-256 to your firebase project.

+ After adding sha-code, now you can run these project.

( If error, you should download google-services.json again maybe due to some changes after adding sha-code ).

## Information , contact for help:
- If you still can't run the project , you can contact me with email: 
- ductrong1313@gmail.com (You're welcome)


## References may be helpful
- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
