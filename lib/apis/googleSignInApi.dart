

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:google_sign_in/google_sign_in.dart';
import '../model/user_model.dart';

class GoogleSignInApi {
  static String? _contactText = "";
  static final _googleSignIn = GoogleSignIn(
    scopes: <String>[
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );
  static LunarUser user = LunarUser();

  static void initialize() {
    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount? account) {
      print("account");
      print(account?.displayName);
      print(account?.email);
      user.setData(account?.displayName,account?.email,account?.photoUrl);
      print(user.email);
      print(user.displayName);
      if (account != null) {
        _handleGetContact(account);
      }
    });
    print("Inside google api");
    print(user);
    _googleSignIn.signInSilently();
  }


  static Future<GoogleSignInAccount?> login() => _googleSignIn.signIn();

  static Future logout() => _googleSignIn.disconnect();

  LunarUser getCurrentUser(){
    var account = _googleSignIn.currentUser;
    LunarUser user = LunarUser();
    if(account != null){
      print("Get current");
      user.setData(user.displayName,user.email,user.photoURL);
    }
    else{
      print("google sign current user is null.");
      user.setData("Unknown","example@gmail.com","");
    }
    return user;
  }

  static Future<void> _handleGetContact(GoogleSignInAccount user) async {

      _contactText = 'Loading contact info...';
    final http.Response response = await http.get(
      Uri.parse('https://people.googleapis.com/v1/people/me/connections'
          '?requestMask.includeField=person.names'),
      headers: await user.authHeaders,
    );
    if (response.statusCode != 200) {

        _contactText = 'People API gave a ${response.statusCode} '
            'response. Check logs for details.';
      print('People API ${response.statusCode} response: ${response.body}');
      return;
    }
    final Map<String, dynamic> data =
    json.decode(response.body) as Map<String, dynamic>;
    final String? namedContact = _pickFirstNamedContact(data);
      if (namedContact != null) {
        _contactText = 'I see you know $namedContact!';
      } else {
        _contactText = 'No contacts to display.';
      }
  }

  static String? _pickFirstNamedContact(Map<String, dynamic> data) {
    final List<dynamic>? connections = data['connections'] as List<dynamic>?;
    final Map<String, dynamic>? contact = connections?.firstWhere(
          (dynamic contact) => contact['names'] != null,
      orElse: () => null,
    ) as Map<String, dynamic>?;
    if (contact != null) {
      final Map<String, dynamic>? name = contact['names'].firstWhere(
            (dynamic name) => name['displayName'] != null,
        orElse: () => null,
      ) as Map<String, dynamic>?;
      if (name != null) {
        return name['displayName'] as String?;
      }
    }
    return null;
  }

}