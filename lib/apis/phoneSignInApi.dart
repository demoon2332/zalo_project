
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../model/user_model.dart';


class PhoneSignIn {
  static final FirebaseAuth _auth = FirebaseAuth.instance;
  var _verificationId;
  late int resendingToken;
  String error = "";

  static LunarUser user = LunarUser();

  static Future logout() => _auth.signOut();

  LunarUser getCurrentUser(){
    var acc = FirebaseAuth.instance.currentUser;
    acc?.displayName != null ? user.displayName = acc?.displayName : user.displayName = "Unknown";
    acc?.photoURL != null ? user.photoURL = acc?.photoURL : user.photoURL = "assets/images/user.png";
    acc?.phoneNumber != null ? user.email = acc?.phoneNumber : user.email = acc?.email;
    return user;
    // print("google sign current user is null.");
    // return null;
  }
}