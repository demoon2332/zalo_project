mixin AccountValidator {
  String? validateEmail(String? email) {
    if (email!.isEmpty) {
      //return 'Email address is required.';
      return null;
    }
    final regex = RegExp('[^@]+@[^\.]+\..+');
    if (!regex.hasMatch(email)) {
      //return 'Enter your email';
      return null;
    }
    else
      return null;
  }

  String? validatePhone(String? phone){
    if(phone!.isEmpty){
      return 'Phone is required.';
    }
    if(phone.length < 6){
      return 'Phone number must longer than 5 digits.';
    }
    else if(int.tryParse(phone)==null){
      return "Phone shouldn't contain letter or any special characters.";
    }
    else{
      return null;
    }
  }
  String? validateName(String? phone){
    if(phone!.isEmpty){
      return 'Name is required.';
    }
    else {
      return null;
    }
  }


  String? validatePass(String? pass){
    String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    if(regExp.hasMatch(pass!))
      return null;
    else
      return null;
    //   return "Password must be longer than 8 and include"
    //   +"\n At least 1 uppercase character"
    // +"\n At least 1 lowercase character"
    // +"\n At least 1 special character.";
  }
}