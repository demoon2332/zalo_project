import 'package:midterm/ui/newfeed/newfeed_screen.dart';
import 'package:midterm/ui/user/user_profile.dart';

import '../ui/all_ui.dart';


class LunarRouter{
  static final routes = {
    'splash_screen': (context) => SplashScreen(),
    'home': (context) => HomeScreen(),
    'login': (context) => LoginScreen(),
    'register': (context) => RegisterScreen(),
    'account': (context) => ProfileScreen(),
    'newfeed': (context) => NewFeedScreen()
  };
}
