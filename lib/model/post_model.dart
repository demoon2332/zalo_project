class Post{
  late final String id;
  late final String profileImageUrl;
  late final String username;
  late final String time;
  late final String content;
  late final String likes;
  late final String comments;
  late final String shares;

  Post({
    required this.id,
    required this.profileImageUrl,
    required this.username,
    required this.time,
    required this.content,
    required this.likes,
    required this.comments,
    required this.shares
  });

  Post.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    profileImageUrl = json['profileImageUrl'];
    username = json['username'];
    time = json['time'];
    content = json['content'];
    likes = json['likes'];
    shares = json['shares'];
    comments = json['comments'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['profileImageUrl'] = this.profileImageUrl;
    data['username'] = this.username;
    data['time'] = this.time;
    data['content'] = this.content;
    data['likes'] = this.likes;
    data['comments'] = this.comments;
    data['shares'] = this.shares;

    return data;
  }
}
