import 'package:flutter/cupertino.dart';

class LunarUser extends ChangeNotifier{
  String? displayName;
  String? email; // email can be replace by phone
  String? photoURL;


  LunarUser({this.displayName,this.email,this.photoURL});

  setData(String? displayName,String? email,String? photoURL){
    this.displayName = displayName;
    this.email = email;
    this.photoURL = photoURL;
  }

  LunarUser.fromJson(Map<String, dynamic> json)
  {
    displayName = json['displayName'];
    photoURL = json['photoURL'];
    email = json['email'];
  }

  Map<String, dynamic> toJson()
  {
    final Map<String, dynamic> mapData = new Map<String, dynamic>();

    mapData['displayName'] = displayName;
    mapData['email'] = email;
    mapData['photoURL'] = photoURL;

    return mapData;

  }

}