class Room {
  int? id;
  String? title;
  String? lastMessage;
  String? lastActive;
  String? image;
  bool? isActive;


  Room(
      {this.id = 0,
        this.title = '',
        this.lastMessage = '',
        this.lastActive = '',
        this.image = '',
        this.isActive = false});

  Room.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    lastMessage = json['lastMessage'];
    lastActive = json['lastActive'];
    image = json['image'];
    isActive = json['isActive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['lastMessage'] = this.lastMessage;
    data['lastActive'] = this.lastActive;
    data['image'] = this.image;
    data['isActive'] = this.isActive;

    return data;
  }
}


