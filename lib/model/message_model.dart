// enum MessageType { text, audio, image, video }
// enum MessageStatus { not_sent, not_seen, seen }

class Message {
  int? roomId;
  String? text;
  String? messageType;
  String? messageStatus;
  bool? isSender;

  Message({
    this.roomId = 1,
    this.text = '',
    required this.messageType,
    required this.messageStatus,
    required this.isSender,
  });

  Message.fromJson(Map<String, dynamic> json) {
    roomId = json['roomId'];
    text = json['text'];
    messageType = json['messageType'];
    messageStatus = json['messageStatus'];
    isSender = json['isSender'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['roomId'] = roomId;
    data['text'] = text;
    data['messageType'] = messageType;
    data['messageStatus'] = messageStatus;
    data['isSender'] = isSender;
    return data;
  }
}

// List messagesList = [
//   Message(
//     text: "Hi Sajol,",
//     messageType: MessageType.text,
//     messageStatus: MessageStatus.seen,
//     isSender: false,
//   ),
//   Message(
//     text: "Hello, How are you?",
//     messageType: MessageType.text,
//     messageStatus: MessageStatus.seen,
//     isSender: true,
//   ),
//   Message(
//     text: "Hi, are you there",
//     messageType: MessageType.audio,
//     messageStatus: MessageStatus.seen,
//     isSender: false,
//   ),
//   Message(
//     text: "Don't tell me that you lost my ship.",
//     messageType: MessageType.video,
//     messageStatus: MessageStatus.seen,
//     isSender: true,
//   ),
//   Message(
//     text: "Sorry can you send it again.",
//     messageType: MessageType.text,
//     messageStatus: MessageStatus.not_sent,
//     isSender: true,
//   ),
//   Message(
//     text: "This looks great man!!",
//     messageType: MessageType.text,
//     messageStatus: MessageStatus.not_seen,
//     isSender: false,
//   ),
//   Message(
//     text: "Glad you like it",
//     messageType: MessageType.text,
//     messageStatus: MessageStatus.not_seen,
//     isSender: true,
//   ),
// ];