class Call {
  int? id;
  String? title;
  String? calledAt;
  String? image;
  bool? isActive;


  Call(
      {this.id = 0,
        this.title = '',
        this.calledAt = '',
        this.image = '',
        this.isActive = false});

  Call.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    calledAt = json['calledAt'];
    image = json['image'];
    isActive = json['isActive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['lastActive'] = this.calledAt;
    data['image'] = this.image;
    data['isActive'] = this.isActive;

    return data;
  }
}

