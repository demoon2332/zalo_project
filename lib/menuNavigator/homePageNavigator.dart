import 'package:flutter/material.dart';

class HomePageNav extends ChangeNotifier{
  int _pageIndex = 0;

  HomePageNav();

  int getPageIndex()=> _pageIndex;

  void changeTab(int index){
    _pageIndex = index;
    notifyListeners();
  }
}