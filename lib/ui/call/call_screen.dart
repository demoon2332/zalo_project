import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../constants.dart';
import 'components/ripple_animation.dart';

class CallScreen extends StatefulWidget {
  final String name;
  const CallScreen({Key? key, required this.name}) : super(key: key);

  @override
  _CallScreenState createState() => _CallScreenState();
}

class _CallScreenState extends State<CallScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  String time = "Connecting..";

  @override
  void dispose() {
    super.dispose();
  }

  void StartAnimation() {
    controller.stop();
    controller.reset();
    controller.repeat(period: const Duration(seconds: 1));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller = AnimationController(vsync: this);
    StartAnimation();
  }

  @override
  Widget build(BuildContext context) {
    var vh = MediaQuery.of(context).size.height;
    var vw = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: const Color(0xff64b5f6),
          padding: const EdgeInsets.all(16),
          child: Center(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: CustomPaint(
                  painter: RipplePainter(controller),
                  child: CircleAvatar(
                    radius: 24,
                    child: Image.asset(
                      'assets/images/user.png',
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.4,
                    ),
                  ),
                ),
              ),
              Center(
                child: Text(widget.name),
              ),
              Text(
                time,
                style: TextStyle(color: Colors.white, fontSize: 23),
              ),

              Padding(
                padding: EdgeInsets.fromLTRB(vw * 0.1,vh* 0.1,vw * 0.1, 0),
                child: Center(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton(onPressed: () {hangUp();}, child: Icon(Icons.speaker)),
                      ElevatedButton(onPressed: (){}, child: Image.asset('assets/images/phone_off_icon.png',
                        width: vw * 0.15 ,height: vw* 0.15,)
                      ),
                      ElevatedButton(onPressed: (){}, 
                      child: Container(
                        child: Icon(Icons.video_call),
                        decoration: BoxDecoration(
                        color: successColor,
                        shape: BoxShape.circle,
                        border: Border.all(
                            color: Theme.of(context).scaffoldBackgroundColor,
                            width: 3),
                      ),
                      )),
                    ],
                  ),
                ),
              )
            ],
          )),
        ),
      ),
    );
  }
  void hangUp(){
    controller.stop();
    setState(() {
      time = "Disconnecting...";
    });
    Future.delayed(const Duration(milliseconds: 2000), (){
      Navigator.of(context).pop();
    });
  }
}
