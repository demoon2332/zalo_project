
import 'package:flutter/material.dart';
import '../../../model/call_model.dart';
import '../../../model/room_model.dart';
import '../../constants.dart';
import '../call_screen.dart';

class CallCard extends StatelessWidget {
  const CallCard({
    Key? key,
    required this.call,
    required this.press,
  }) : super(key: key);

  final Call call;
  final VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: press,
      child: Padding(
        padding: const EdgeInsets.symmetric(
            horizontal: defaultPadding, vertical: defaultPadding * 0.75),
        child: Row(
          children: [
            Stack(
              children: [
                CircleAvatar(
                  radius: 24,
                    backgroundImage: AssetImage("assets/images/user.png"),
                  //backgroundImage: AssetImage(room.image!),
                ),
                if (call.isActive!)
                  Positioned(
                    right: 0,
                    bottom: 0,
                    child: Container(
                      height: 16,
                      width: 16,
                      decoration: BoxDecoration(
                        color: successColor,
                        shape: BoxShape.circle,
                        border: Border.all(
                            color: Theme.of(context).scaffoldBackgroundColor,
                            width: 3),
                      ),
                    ),
                  )
              ],
            ),
            Expanded(
              child: Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: defaultPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      call.title!,
                      style:
                      TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(height: 8),
                    // Opacity(
                    //   opacity: 0.64,
                    //   child: Text(
                    //     room.lastMessage!,
                    //     maxLines: 1,
                    //     overflow: TextOverflow.ellipsis,
                    //   ),
                    // ),
                  ],
                ),
              ),
            ),
            Opacity(
              opacity: 0.64,
              child: InkWell(
                child: Icon(Icons.call),
                onTap: (){
                 Navigator.of(context).push(MaterialPageRoute(
                builder: (_) => CallScreen(
                      name: call.title!,
                    )));
                },
              ),
            ),
            SizedBox(
              width: 8,
            ),
            Opacity(
              opacity: 0.64,
              child: InkWell(
                child: Icon(Icons.video_call),
                onTap: (){
                 Navigator.of(context).push(MaterialPageRoute(
                builder: (_) => CallScreen(
                      name: call.title!,
                    )));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}