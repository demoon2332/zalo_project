
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RippleAnimation extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _RippleAnimationState();
  }
}

class _RippleAnimationState extends State<RippleAnimation> with SingleTickerProviderStateMixin{

  late AnimationController controller;

  @override
  void initState(){
    super.initState();
    controller = AnimationController(vsync: this);
    StartAnimation();
  }

  @override
  void dispose(){
    super.dispose();

  }

  void StartAnimation(){
    controller.stop();
    controller.reset();
    controller.repeat(period: Duration(seconds: 1));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: CustomPaint(
          painter: RipplePainter(controller),
          child: Container(),
        ),
      ),
    );
  }
}

class RipplePainter extends CustomPainter {
  final Animation<double> animation;
  RipplePainter(this.animation) : super(repaint: animation);

  void Circle (Canvas canvas,Rect rect,double value){

    double opacity = (1.0 - (value/4.0)).clamp(.0,1.0);
    double size = rect.width/2;
    double area = size * size;
    double radius = sqrt(area * value/4);
    Color color = Colors.blue.withOpacity(opacity);

    final Paint paint = Paint()..color = color;

    canvas.drawCircle(rect.center, radius, paint);
  }

  @override
  void paint(Canvas canvas, Size size) {
    Rect rect =  Rect.fromLTRB(0.0,0.0,size.width,size.height);

    for(int wave=3;wave >=0;wave--){
      Circle(canvas,rect,wave+animation.value);
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}