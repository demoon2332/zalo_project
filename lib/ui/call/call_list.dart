
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../../model/call_model.dart';
import '../constants.dart';
import '../messages/messages_screen.dart';
import 'components/call_card.dart';

class CallList extends StatefulWidget {
  const CallList({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _CallListState();
  }

}


class _CallListState extends State<CallList>{
  late List<dynamic> jsonData;
  late List<Call> callList = <Call>[];

  Future _getData() async {
    final String response = await rootBundle.loadString('assets/call.json');
    jsonData = await json.decode(response) as List<dynamic>;
    for (var i = 0; i < jsonData.length; i++) {
      callList.add(Call.fromJson(jsonData[i]));
    }
    // return roomList;
  }
  
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Expanded(
            child: FutureBuilder<dynamic>(
                future: _getData(),
                builder:
                    (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    print("length");
                    print(callList.length);
                    return ListView.builder(
                      itemCount: callList.length,
                      itemBuilder: (context, index) => CallCard(
                        call: callList[index],
                        press: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MessagesScreen(
                              roomName: callList[index].title!,
                            ),
                          ),
                        ),
                      ),
                    );
                  }
                  else if(snapshot.connectionState == ConnectionState.waiting){
                    return const Center(
                      child: SpinKitDoubleBounce(
                        size: 140,
                        duration: Duration(seconds: 2),
                        color: Colors.cyan),
                    );
                  }
                  else {
                    return const Center(
                      child: SpinKitFadingCircle(
                        size: 140,
                        duration: Duration(seconds: 2),
                        color: Colors.cyan),
                    );
                  }
                }),
          ),
        ],
      ),
    ));
  }

}