import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:sizer/sizer.dart';

class StoriesWidget extends StatelessWidget {
  late List<dynamic> jsonData;
  late List<dynamic> data; // display on screen

  Future _getData() async {
    final String response =
        await rootBundle.loadString('assets/stories.json');
    jsonData = await json.decode(response) as List<dynamic>;
    data = jsonData;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 220.0,
      padding: EdgeInsets.symmetric(vertical: 15.0),
      child: FutureBuilder(
        future: _getData(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: data.length,
              itemBuilder: (context, index) => story(data[index]['url'])
            );
          } else {
            return const Center(
              child: SpinKitDoubleBounce(
                  size: 140,
                  duration: Duration(seconds: 2),
                  color: Colors.cyan),
            );
          }
        },
      ),
      // child: ListView(
      //   scrollDirection: Axis.horizontal,
      //   children: <Widget>[
      //     SizedBox(width: 15.0),
      //     Container(
      //       width: 120.0,
      //       decoration: BoxDecoration(
      //           borderRadius: BorderRadius.circular(10.0),
      //           image: DecorationImage(image: AssetImage('assets/story1.jpg'), fit: BoxFit.cover)
      //       ),
      //     ),
      //   ],
      // ),
    );
  }

  Widget story(String url) {
    return Container(
        width: 60.w,
        height: 30.h,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            image:
                DecorationImage(image: AssetImage(url), fit: BoxFit.cover)));
  }
}
