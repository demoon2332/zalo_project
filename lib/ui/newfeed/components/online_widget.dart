import 'package:flutter/material.dart';

import '../../call/call_screen.dart';

class OnlineWidget extends StatelessWidget {
  const OnlineWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 75.0,
      padding: EdgeInsets.symmetric(vertical: 15.0),
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          SizedBox(width: 15.0),
          // Container(
          //   padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 2.0),
          //   decoration: BoxDecoration(
          //       borderRadius: BorderRadius.circular(40.0),
          //       border: Border.all(
          //           width: 1.0,
          //           color: Colors.blue
          //       )
          //   ),
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.spaceAround,
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: <Widget>[
          //       IconButton(
          //         icon: Icon(Icons.video_call, size: 30.0, color: Colors.purple),
          //         onPressed: () {
          //           Navigator.of(context).push(MaterialPageRoute(
          //               builder: (_) => CallScreen(
          //                 name: "Call",
          //               )));
          //         },
          //       ),
          //
          //       SizedBox(width: 5.0),
          //       Column(
          //         mainAxisAlignment: MainAxisAlignment.center,
          //         children: <Widget>[
          //           Text('Create', style: TextStyle(color: Colors.blue)),
          //           Text('Room', style: TextStyle(color: Colors.blue)),
          //         ],
          //       )
          //     ],
          //   ),
          // ),

          SizedBox(width: 15.0),
          onlineCard('assets/images/user/user2.jpg'),
          SizedBox(width: 15.0),
          onlineCard('assets/images/user/user3.jpg'),
          SizedBox(width: 15.0),
        ],
      ),
    );
  }

  Widget onlineCard(String imageUrl){
    return Stack(
      children: <Widget>[
        CircleAvatar(
          radius: 22.0,
          backgroundImage: AssetImage(imageUrl),
        ),
        Positioned(
          right: 1.0,
          bottom: 1.0,
          child: CircleAvatar(
            radius: 6.0,
            backgroundColor: Colors.green,
          ),
        ),
      ],
    );
  }
}


