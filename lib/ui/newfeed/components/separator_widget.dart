import 'package:flutter/material.dart';

class SeparatorWidget extends StatelessWidget {
  const SeparatorWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xDCE5E5E5),
      width: MediaQuery.of(context).size.width,
      height: 11.0,
    );
  }
}

