import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:http/http.dart';
import 'package:midterm/ui/newfeed/components/post_widget.dart';
import 'package:sizer/sizer.dart';
import '../../model/post_model.dart';
import '../components/responsive.dart';
import 'components/online_widget.dart';
import 'components/stories_widget.dart';
import 'components/separator_widget.dart';
import 'components/write_something_widget.dart';

class NewFeedScreen extends StatefulWidget {
  const NewFeedScreen({Key? key}) : super(key: key);

  @override
  _NewFeedState createState() => _NewFeedState();
}

class _NewFeedState extends State<NewFeedScreen> {
  late List<dynamic> jsonData;
  late List<Post> roomList = <Post>[];
  final TrackingScrollController _trackingScrollController =
      TrackingScrollController();

  @override
  void dispose() {
    _trackingScrollController.dispose();
    super.dispose();
  }

  Future _getData() async {
    final String response = await rootBundle.loadString('assets/room.json');
    jsonData = await json.decode(response) as List<dynamic>;
    for (var i = 0; i < jsonData.length; i++) {
      roomList.add(Post.fromJson(jsonData[i]));
    }
    // return roomList;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: SafeArea(
          child: Scaffold(
            body: Responsive(
              mobile: _HomeScreenMobile(
                  scrollController: _trackingScrollController),
              desktop: _HomeScreenDesktop(
                  scrollController: _trackingScrollController),
            ),
          ),
        ));
  }
}

class _HomeScreenMobile extends StatelessWidget {
  final TrackingScrollController scrollController;
  late List<dynamic> jsonData;
  late List<Post> postList = <Post>[];

  Future _getData() async {
    final String response = await rootBundle.loadString('assets/post.json');
    jsonData = await json.decode(response) as List<dynamic>;
    for (var i = 0; i < jsonData.length; i++) {
      postList.add(Post.fromJson(jsonData[i]));
    }
    // return roomList;
  }

  _HomeScreenMobile({
    Key? key,
    required this.scrollController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      primary: true,
        children: <Widget>[
          WriteSomethingWidget(),
          SeparatorWidget(),
          OnlineWidget(),
          SeparatorWidget(),
          StoriesWidget(),
          // for(Post post in posts) Column(
          //   children: <Widget>[
          //     SeparatorWidget(),
          //     PostWidget(post: post),
          //   ],
          // ),
          FutureBuilder<dynamic>(
              future: _getData(),
              builder:
                  (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if(postList.isNotEmpty){
                    return ListView.builder(
                      primary: false,
                      shrinkWrap: true,
                      itemCount: postList.length,
                      itemBuilder: (context, index) => PostWidget(post: postList[index])
                    );
                  }
                  else{
                    return Center(
                      child: Text("no data"),
                    );
                  }
                }
                else if(snapshot.connectionState == ConnectionState.waiting){
                  return const Center(
                    child: SpinKitDoubleBounce(
                        size: 140,
                        duration: Duration(seconds: 2),
                        color: Colors.cyan),
                  );
                }
                else {
                  return const Center(
                    child: SpinKitFadingCircle(
                        size: 140,
                        duration: Duration(seconds: 2),
                        color: Colors.cyan),
                  );
                }
              }),
          SeparatorWidget(),
        ],
    );
  }

  Widget _PostWidget(Post post) {
    return Expanded(
        child: Column(children: <Widget>[
          SeparatorWidget(),
          PostWidget(post: post),
        ]));
  }
}

class _HomeScreenDesktop extends StatelessWidget {
  final TrackingScrollController scrollController;

  const _HomeScreenDesktop({
    Key? key,
    required this.scrollController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
