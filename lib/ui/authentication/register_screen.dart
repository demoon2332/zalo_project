import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'verify_screen.dart';
import '../../validator/accountValidator.dart';
import '../home_screen.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return RegisterScreenState();
  }
}

class RegisterScreenState extends State<StatefulWidget> with AccountValidator {
  final formKey = GlobalKey<FormState>();
  final phoneController = TextEditingController();

  @override
  void dispose() {
    phoneController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Center(
            child: Text('Create an account'),
          ),
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(32),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  Row(
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      Expanded(child: phoneField())
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(height: 20),
                      Padding(
                        padding: const EdgeInsets.all(20),
                        child: registerButton(context),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: const [
                      SizedBox(height: 20),
                      Padding(
                        padding: EdgeInsets.all(20),
                        child: Text('We will send SMS message to your phone number to verify.'),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        )
      ,floatingActionButton: FloatingActionButton(
      onPressed: () {  },
      child: const Icon(Icons.arrow_right_alt_sharp),
    ),//body
    );
  }

  Widget phoneField() {
    return TextFormField(
      controller: phoneController,
      decoration: const InputDecoration(
        icon: Icon(Icons.person),
        border: UnderlineInputBorder(),
        labelText: 'phone address',
        hintText: 'Enter phone',
      ),
      autocorrect: false,
      autofocus: false,
      textCapitalization: TextCapitalization.none,
      keyboardType: TextInputType.phone,
      validator: validatePhone,
    );
  }

  Widget registerButton(BuildContext context) {
    return ElevatedButton(
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => const VerifyScreen()));
        },
        child: const Padding(
          padding: EdgeInsets.all(6),
          child: Text('Register'),
        ));
  }
}
