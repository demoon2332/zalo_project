import 'package:carousel_slider/carousel_slider.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'edit_phone_screen.dart';
import '../../ui/all_ui.dart';
import 'select_country.dart';
import '../constants.dart';
import '../../apis/googleSignInApi.dart';
import '../../validator/accountValidator.dart';
import 'package:sizer/sizer.dart';

// final GoogleSignIn _googleSignIn = GoogleSignIn();

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LoginScreenState();
  }
}

class LoginScreenState extends State<StatefulWidget> with AccountValidator {
  final formKey = GlobalKey<FormState>();
  final phoneController = TextEditingController();
  // default phone format for Vietnam phone number
  Map<String, dynamic> data = {"name": "Vietnam", "code": "+84"};
  Map<String, dynamic>? dataResult;
  final _auth = FirebaseAuth.instance;
  GoogleSignInAccount? _currentUser;
  int _current = 0;

  @override
  void initState() {
    print("Current User in login screen");
    print(_auth.currentUser);
    GoogleSignInApi.initialize();
    setState(() {});
  }

  @override
  void dispose() {
    phoneController.dispose();
    super.dispose();
  }

  final List<String> images = [
    "assets/images/slide_1.png",
    "assets/images/slide_2.png",
    "assets/images/slide_3.png",
    "assets/images/zalo_icon.jpg"
  ];
  final List<String> titles = [
    "Stable Call Video",
    "Convenient Group Chat",
    "Send Photo Quickly",
    "Welcome To Zalo",
  ];

  final List<String> details = [
    "Realtime chat with beautiful images,standard sound under all network conditions",
    "Exchange, keep in touch with families, friends and colleagues",
    "Share high-quality images to friends and relatives rapidly and easily",
    "The journey begins here"
  ];




  @override
  Widget build(BuildContext context) {
    var vh = MediaQuery.of(context).size.height;
    var vw = MediaQuery.of(context).size.width;
    GlobalKey<CarouselSliderState> _sliderKey = GlobalKey();
    final CarouselController _controller = CarouselController();

    final List<Widget> sliders = images.map((item)=> ClipRRect(
            borderRadius:
            BorderRadius.all(Radius.circular(8.0)),
            child: Column(
              crossAxisAlignment:
              CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                  flex: 2,        
                  child: Image.asset(
                    item,
                    fit: BoxFit.scaleDown,
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Column(children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: vh* 0.01, horizontal: vw * 0.01),
                    child: Text(
                      titles[images.indexOf(item)],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 14.sp,
                          color: Colors.black87,fontWeight: FontWeight.bold),
                    ),
                ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: vh* 0.01, horizontal: vw * 0.01),
                    child: Text(
                      details[images.indexOf(item)],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 14.sp,
                          color: Colors.black87),
                    ),
                  )      
                  ]),)
              ],
            ),
          ),   
    ).toList();

    final List<Widget> slider2 = images.map((item)=> Expanded(child: Container(color: Colors.black)),).toList();

    return Scaffold(
      resizeToAvoidBottomInset: false,
      // appBar: AppBar(
      //   title: const Center(
      //     child: Text('Login'),
      //   ),
      // ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Stack(
            children: <Widget>[
              Opacity(
                opacity: 0.3,
                child: Image.asset(
                  "assets/images/login_bg.png",
                  fit: BoxFit.cover,
                  width: vw,
                  height: vh,
                ),
              ),
              Column(
                children: <Widget>[
                  Center(
                    child: Text(
                      'Zalo',
                      style: TextStyle(
                        fontSize: 30.sp,
                        color: primaryColor2,
                        fontWeight: FontWeight.w700,
                        shadows: const <Shadow>[
                          Shadow(
                            offset: Offset(1.0, 1.0),
                            blurRadius: 3.0,
                            color: lightGray,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Expanded(child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 8.0),
                            child: CarouselSlider(
                                carouselController: _controller,
                                items: sliders, // black children 
                                options: CarouselOptions(
                                  height: 55.h,
                                  scrollDirection: Axis.horizontal,
                                  autoPlay: true,
                                  enlargeCenterPage: false,
                                  viewportFraction: 1,
                                    onPageChanged: (index, reason) {
                                      setState(() {
                                        _current = index;
                                      });
                                    })),
                          ),),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: images.asMap().entries.map((entry) {
                                return GestureDetector(
                                  onTap: () => _controller.animateToPage(entry.key),
                                  child: Container(
                                    width: 12.0,
                                    height: 12.0,
                                    margin: EdgeInsets.symmetric(
                                        vertical: 8.0, horizontal: 4.0),
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: (Theme.of(context).brightness ==
                                            Brightness.dark
                                            ? Colors.white
                                            : Colors.black)
                                            .withOpacity(
                                            _current == entry.key ? 0.9 : 0.4)),
                                  ),
                                );
                              }).toList(),                          
                          )
                        ],
                      ), 
                    ),                   
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: vh * 0.05),
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: loginWithPhone(context),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: vh * 0.05),
                      Padding(
                        padding: const EdgeInsets.all(10),
                        child: loginWithGoogle(context),
                      )
                    ],
                  ),

                  // const Flexible(
                  //   flex: 1,
                  //   child: Text(
                  //     "You will receive OTP message sent to your phone number.",
                  //     style: TextStyle(color: Colors.grey, fontSize: 16,),
                  //   ),
                  // ), //const SizedBox(height: 20),
                ],
              )
            ],
          ),
          // Padding(
          //   padding: const EdgeInsets.all(4),
          //   child: Row(
          //     children: [
          //       Text(
          //         data['code'],
          //         style:
          //             TextStyle(fontSize: 16.sp, color: primaryColor),
          //       ),
          //       Expanded(child: phoneField()),
          //     ],
          //   ),
          // ),

          // Row(
          //   crossAxisAlignment: CrossAxisAlignment.center,
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: [
          //     TextButton(
          //       child: Text(
          //           "Click here to change country code",
          //           style: TextStyle(
          //               color: primaryColor, fontSize: 14.0.sp)),
          //       onPressed: () async {
          //         dataResult = await Navigator.push(
          //             context,
          //             MaterialPageRoute(
          //                 builder: (context) => SelectCountry()));
          //         setState(() {
          //           if (dataResult != null) data = dataResult!;
          //         });
          //       },
          //     ),
          //   ],
          // ),

          // Padding(
          //   padding: const EdgeInsets.all(10),
          //   child: loginButton(context),
          // ),
        ),
      ), //body
    );
  }

  Widget phoneField() {
    return TextFormField(
      controller: phoneController,
      decoration: InputDecoration(
        icon: Icon(Icons.phone),
        label: const Text(
          'Phone',
          style: TextStyle(color: Colors.grey),
        ),
        border: OutlineInputBorder(
            borderSide: BorderSide(color: primaryColor),
            borderRadius: BorderRadius.circular(12)),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green),
            borderRadius: BorderRadius.circular(12)),
        hintText: 'Enter phone number',
      ),
      autocorrect: false,
      autofocus: false,
      textCapitalization: TextCapitalization.none,
      keyboardType: TextInputType.phone,
      validator: validatePhone,
    );
  }


  Widget loginButton(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: successColor, shape: StadiumBorder()),
        onPressed: () {
          print("Phone: " + phoneController.text);
          Navigator.of(context).push(MaterialPageRoute(
              builder: (_) => VerifyScreen(
                    phoneNumber: data['code']! + phoneController.text,
                  )));
        },
        child: const Padding(
          padding: EdgeInsets.all(6),
          child: Text(
            'Continous',
          ),
        ));
  }

  Widget loginWithGoogle(BuildContext context) {
    return Center(
      child: Column(
        children: [
          GestureDetector(
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: primaryColor2, shape: StadiumBorder()),
                  onPressed: signIn,
                  child: Row(
                    children: [
                      Image.asset(
                        "assets/images/google_btn.png",
                        width: 50,
                        height: 30,
                      ),
                      Text(
                        'Sign In with Google Account',
                        style: TextStyle(fontSize: 14.0.sp),
                      ),
                    ],
                  )))
        ],
      ),
    );
    // return ElevatedButton(
    //
    //     onPressed: () {
    //       print("Phone: " + phoneController.text);
    //       print("Pass: " + passController.text);
    //       Navigator.of(context)
    //           .push(MaterialPageRoute(builder: (_) => HomeScreen()));
    //     },
    //     child: const Padding(
    //       padding: EdgeInsets.all(6),
    //       child: Text('Login'),
    //     ));
  }

  Widget loginWithPhone(BuildContext context) {
    return Center(
      child: Column(
        children: [
          GestureDetector(
              child: Row(
            children: [
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: primaryColor2, shape: StadiumBorder()),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EditPhoneScreen())
                    );
                  },
                  child: Text(
                    'Sign In with Phone number',
                    style: TextStyle(fontSize: 14.0.sp),
                  ))
            ],
          ))
        ],
      ),
    );
  }

  Widget registerButton(BuildContext context) {
    return InkWell(
        child: ElevatedButton(
            onPressed: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (_) => const RegisterScreen()));
            },
            child: const Padding(
              padding: EdgeInsets.all(6),
              child: Text('Register an account'),
            )));
  }

  Future signIn() async {
    try {
      print("step 1");
      await GoogleSignInApi.login().then((value) => {
            setState(() {
              print("step 2");
              print(value.toString());
              _currentUser = value;
            })
          });
      print("log in");
      if (GoogleSignInApi().getCurrentUser() != null)
        Navigator.popAndPushNamed(context, 'home');
    } catch (error) {
      print("Error in login screen");
      print(error);
      print('end');
    }
  }
}
