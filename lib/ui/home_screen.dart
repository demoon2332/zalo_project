
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:midterm/apis/phoneSignInApi.dart';
import 'package:midterm/menuNavigator/homePageNavigator.dart';
import 'package:midterm/ui/call/call_list.dart';
import 'package:midterm/ui/newfeed/newfeed_screen.dart';
import 'package:midterm/ui/user/user_profile.dart';
import 'package:provider/provider.dart';
import '../apis/googleSignInApi.dart';
import '../model/room_model.dart';
import './rooms/body.dart';
import 'messages/messages_screen.dart';
import 'package:badges/badges.dart';

//NAV
import '../menuNavigator/homePageNavigator.dart';

import '../model/user_model.dart';

final FirebaseAuth auth = FirebaseAuth.instance;
late User user;
void inputData() {
  User? user = auth.currentUser;
  final uid = user?.uid;
  final name = user?.displayName;
  print("User name :"+name!);
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  //const HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeScreenState();
  }
}

class HomeScreenState extends State<StatefulWidget> {
  List<Widget> pageList = <Widget>[];
  late LunarUser user;
  var rooms = [
    "banana",
    "strawberry",
    "blueberry",
    "mango",
    "papaya",
    "watermelon",
    "avocado",
    "apple",
  ];

  @override
  void initState(){

    //pageList add:
    pageList.add(const Body());
    pageList.add(const CallList());
    pageList.add(const NewFeedScreen());
    pageList.add(const ProfileScreen());

    if(GoogleSignInApi().getCurrentUser() != null)
      {
        user = GoogleSignInApi().getCurrentUser();
      }
    else{
      user = PhoneSignIn().getCurrentUser();
    }


    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    // double height = MediaQuery.of(context).size.height;
    // double width = MediaQuery.of(context).size.width ;
    return Consumer<HomePageNav>(
      builder: (context,homePageNav,children){
      int tabPage = homePageNav.getPageIndex();
      return SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Zalo'),
            actions: <Widget>[
              IconButton(
                onPressed: () {
                  showSearch(context: context, delegate: DataSearch(rooms));
                },
                icon: const Icon(Icons.search),
              )
            ],
          ),
            body: IndexedStack(
              children: pageList,
              index: tabPage,
            ),
          drawer: zaloDrawer(context,homePageNav),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(icon: Icon(Icons.message), label: 'Message'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.phone), label: 'Call'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.web_sharp), label: 'NewFeed'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.account_box_outlined), label: 'Account'),
            ],
            selectedItemColor: Colors.green,
            onTap: (index){
              homePageNav.changeTab(index);
            },//=>_onItemPageTapped(index, tabPage),
            currentIndex: tabPage,
            elevation: 8,
          )
        ),
      );

    },);
  }


  //drawer (from left)
  Widget zaloDrawer(BuildContext context,HomePageNav homePageNav) {
    String name = "Unknown";
    String email = "example@gmail.com";
    if(GoogleSignInApi.user != null){
      if(GoogleSignInApi.user.displayName !=  null){
      name = GoogleSignInApi.user.displayName!;
      }
      if(GoogleSignInApi.user.email != null){
        email = GoogleSignInApi.user.email!;
      }
    }
    else if(PhoneSignIn.user != null){
      if(PhoneSignIn.user.displayName != null){
        name = PhoneSignIn.user.displayName!;
      }
      if(PhoneSignIn.user.email != null){
        email = PhoneSignIn.user.email!;
      }
    }
  
    return Drawer(
      child: ListView(
        padding: const EdgeInsets.all(5),
        children: <Widget>[
          Container(
            height: 24.0,
            color: Colors.blue[700],
          ),
          Container(
            color: Colors.blue,
            child: ListTile(
              leading: const Icon(
                Icons.account_circle,
                size: 40.0,
              ),
              title:  Text(
                name,
                style: TextStyle(color: Colors.white),
              ),
              subtitle: Text(
                email,
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {},
            ),
          ),
          ListTile(
            leading: Icon(Icons.account_circle),
            title: Text("My Account"),
            onTap: () {
              homePageNav.changeTab(3);
            },
          ),
          ListTile(
            leading: Icon(Icons.account_circle),
            title: Text("New Feed"),
            onTap: () {
              homePageNav.changeTab(2);
            },
          ),
          ListTile(
            leading: Icon(Icons.notifications),
            title: Text("Stories"),
            onTap: () {
              Navigator.popAndPushNamed(context,'stories');
            },
          ),
          ListTile(
            leading: Icon(Icons.logout),
            title: Text("Log Out"),
            onTap: () {
              GoogleSignInApi.logout();
              PhoneSignIn.logout();
              Navigator.popAndPushNamed(context,'login');
            },
          )
        ],
      ),
    );
  }


}

class DataSearch extends SearchDelegate<String> {
  DataSearch(this.rooms);

  List<String> rooms = [];

  var recentRooms = [
    "apple",
    "mango",
    "avocado",
    "strawberry",
  ];

  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
          onPressed: () {
            query = "";
          },
          icon: const Icon(Icons.clear))
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
        onPressed: () {
          close(context, "");
        },
        icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow,
          progress: transitionAnimation,
        ));
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    var suggestionList = query.isEmpty
        ? recentRooms
        : rooms.where((room) => room.startsWith(query)).toList();

    return ListView.builder(
      itemBuilder: (context, index) => ListTile(
        leading: const Image(
          image: AssetImage('assets/images/user.png'),
        ),
        title: RichText(
          text: TextSpan(
              text: suggestionList[index].substring(0, query.length),
              style: const TextStyle(fontWeight: FontWeight.bold,color: Colors.black87),
              children: [
                TextSpan(
                    text: suggestionList[index].substring(query.length),
                    style: const TextStyle(color: Colors.grey))
              ]),
        ),
        onTap: () {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (_) => MessagesScreen(roomName: query)));
        },
      ),
      itemCount: suggestionList.length,
    );
  }
}

class RoomList extends StatefulWidget {
  const RoomList({Key? key}) : super(key: key);


  @override
  State<StatefulWidget> createState() {
    return RoomListState();
  }
}

class RoomListState extends State<RoomList> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      padding: EdgeInsets.zero,
      itemCount: 2,
      itemBuilder: (BuildContext context, int index) {
        return ListTile(
          leading: const CircleAvatar(
            radius: 24,
            backgroundImage: AssetImage("assets/images/user_2.png"),
          ),
          trailing: const Text('15 mins ago'),
          title: Text("Room $index"),
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (_) => const MessagesScreen(roomName: 'apple')));
          },
        );
      },
    );
  }
}
