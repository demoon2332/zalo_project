import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../components/customButton.dart';
import '../constants.dart';
import '../../model/room_model.dart';
import '../messages/messages_screen.dart';
import 'package:flutter/material.dart';
import 'room_card.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  late List<dynamic> jsonData;
  late List<Room> roomList = <Room>[];

  @override
  void initState() {
    super.initState();
  }

  Future _getData() async {
    final String response = await rootBundle.loadString('assets/room.json');
    jsonData = await json.decode(response) as List<dynamic>;
    for (var i = 0; i < jsonData.length; i++) {
      roomList.add(Room.fromJson(jsonData[i]));
    }
    // return roomList;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 10,
          ),
          Container(
            padding: const EdgeInsets.fromLTRB(
                defaultPadding, 0, defaultPadding, defaultPadding),
            color: primaryColor,
            child: Row(
              children: [
                CustomButton(press: () {}, text: "Recent Message"),
                SizedBox(width: defaultPadding),
                CustomButton(
                  press: () {},
                  text: "Message from stranger",
                  isFilled: false,
                ),
              ],
            ),
          ),
          Expanded(
            child: FutureBuilder<dynamic>(
                future: _getData(),
                builder:
                    (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    print("length");
                    print(roomList.length);
                    return ListView.builder(
                      itemCount: roomList.length,
                      itemBuilder: (context, index) => RoomCard(
                        room: roomList[index],
                        press: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MessagesScreen(
                              roomName: roomList[index].title!,
                            ),
                          ),
                        ),
                      ),
                    );
                  }
                  else if(snapshot.connectionState == ConnectionState.waiting){
                    return const Center(
                      child: SpinKitDoubleBounce(
                        size: 140,
                        duration: Duration(seconds: 2),
                        color: Colors.cyan),
                    );
                  }
                  else {
                    return const Center(
                      child: SpinKitFadingCircle(
                        size: 140,
                        duration: Duration(seconds: 2),
                        color: Colors.cyan),
                    );
                  }
                }),
          ),
        ],
      ),
    ));
  }
}
