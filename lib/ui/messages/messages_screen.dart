import 'package:flutter/material.dart';
import 'package:midterm/ui/constants.dart';
import 'package:midterm/ui/messages/components/messages_screen_body.dart';
import '../call/call_screen.dart';
import '../home_screen.dart';

class MessagesScreen extends StatefulWidget {
  const MessagesScreen({Key? key, required this.roomName}) : super(key: key);

  final String roomName;

  @override
  State<StatefulWidget> createState() {
    return MessagesScreenState();
  }
}

class MessagesScreenState extends State<MessagesScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customAppBar(),
      body: MSBody(),
    );
  }

  PreferredSizeWidget customAppBar() {
    return AppBar(
      // leading: IconButton(
      //     onPressed: () {
      //       Navigator.pop(context);
      //     },
      //     icon: AnimatedIcon(
      //       icon: AnimatedIcons.arrow_menu,
      //       progress: _animationController,
      //     )),

      automaticallyImplyLeading: false,
      title: Row(
        children: [
          const BackButton(),
          const CircleAvatar(
            backgroundImage: AssetImage("assets/images/user.png"),
          ),
          const SizedBox(
            width: defaultPadding * 0.8,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(widget.roomName, style: TextStyle(fontSize: 16)),
              Text(
                "Active 5m ago",
                style: TextStyle(fontSize: 12),
              )
            ],
          )
        ],
      ),
      actions: [
        IconButton(
          icon: Icon(Icons.phone),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (_) => CallScreen(
                      name: widget.roomName,
                    )));
          },
        ),
        IconButton(
          icon: Icon(Icons.video_call),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (_) => CallScreen(
                  name: widget.roomName,
                )));
          },
        ),
        const SizedBox(
          width: defaultPadding * 0.6,
        )
      ],
    );
  }
}
