import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:midterm/model/message_model.dart';
import 'package:midterm/ui/constants.dart';
import 'package:midterm/ui/messages/components/message_input.dart';

import 'message.dart';

class MSBody extends StatefulWidget {
  const MSBody({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MSBodyState();
  }
}

class MSBodyState extends State<MSBody> {
  late List<dynamic> jsonData;
  late List<Message> messagesList = <Message>[];

  @override
  void initState() {
    super.initState();
  }

  Future _getData() async {
    final String response = await rootBundle.loadString('assets/message.json');
    jsonData = await json.decode(response) as List<dynamic>;
    for (var i = 0; i < jsonData.length; i++) {
      messagesList.add(Message.fromJson(jsonData[i]));
      
      print(messagesList[i].text);
      print(messagesList[i].messageType);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
            child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: defaultPadding,
                ),
                child: FutureBuilder(
                    future: _getData(),
                    builder: (BuildContext context,
                        AsyncSnapshot<dynamic> snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        return ListView.builder(
                          itemCount: messagesList.length,
                          itemBuilder: (context, index) =>
                              MessageCard(message: messagesList[index]),
                        );
                      } else if (snapshot.connectionState ==
                          ConnectionState.waiting) {
                        return const Center(
                          child: SpinKitDoubleBounce(
                              size: 140,
                              duration: Duration(seconds: 2),
                              color: Colors.cyan),
                        );
                      } else {
                        return const Center(
                          child: SpinKitFadingCircle(
                              size: 140,
                              duration: Duration(seconds: 2),
                              color: Colors.cyan),
                        );
                      }
                    }))),
        MSInputField(),
      ],
    );
  }
}
