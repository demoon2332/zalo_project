

import 'package:flutter/material.dart';
import 'package:midterm/ui/constants.dart';
import '../../../model/message_model.dart';
import 'audio_message.dart';
import 'video_message.dart';
import 'text_message.dart';

class MessageCard extends StatelessWidget{

  const MessageCard({Key? key, required this.message}) : super(key: key);

  final Message message;



  @override
  Widget build(BuildContext context) {
    bool isSender = true;
    if(message.isSender != null){
      if(message.isSender == false){
        isSender = false;
      }
    }

    return Padding(
      padding: const EdgeInsets.only(top: defaultPadding),
      child: Row(
        mainAxisAlignment: isSender ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: [
          if(!isSender)...[
            CircleAvatar(
              radius: 12,
              backgroundImage: AssetImage("assets/images/user.png"),
            ),
            SizedBox(width: defaultPadding * 0.5,),
          ],
          messageContent(message)
        ],
      ),
    );
  }

  Widget messageContent(Message message) {
    switch (message.messageType) {
      case "text":
        return TextMessage(message: message);
      case "audio":
        return AudioMessage(message: message);
      case "video":
        return VideoMessage();
      default:
        return SizedBox();
    }
  }




























}