


import 'package:flutter/material.dart';

import '../../../model/message_model.dart';
import '../../constants.dart';

class TextMessage extends StatelessWidget{



  final Message? message;

  const TextMessage({Key? key, this.message}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    double opacity = 1;
    if(message?.isSender != null){
      if(message?.isSender == false){
        opacity = 0.4;
      }
    }
    return Container(
      // color: MediaQuery.of(context).platformBrightness == Brightness.dark
      //     ? Colors.white
      //     : Colors.black,
      padding: const EdgeInsets.symmetric(
        horizontal: defaultPadding * 0.75,
        vertical: defaultPadding / 2,
      ),
      decoration: BoxDecoration(
        color: primaryColor.withOpacity(opacity),
        borderRadius: BorderRadius.circular(30),
      ),
      child: Text(
        message?.text == null ? "" : "${message?.text}",
        style: TextStyle(
          color: message?.isSender != null
              ? Color.fromARGB(255, 255, 254, 254)
              : Theme.of(context).textTheme.bodyText1!.color,
        ),
      ),
    );
  }
}