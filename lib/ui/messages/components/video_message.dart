


import 'package:flutter/material.dart';
import 'package:midterm/ui/constants.dart';

class VideoMessage extends StatelessWidget{
  const VideoMessage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.45,
      child: AspectRatio(
        aspectRatio: 1.6,
        child: Stack(
          alignment: Alignment.center,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.asset("assets/images/zalo_icon.jpg"),
            ),
            Container(
              height: 25,
              width: 25,
              decoration: const BoxDecoration(
                color: primaryColor,
                shape: BoxShape.circle,
              ),
              child: const Icon(
                Icons.play_arrow_sharp,
                size: 16,
                color: Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }

  
}