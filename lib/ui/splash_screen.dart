import 'package:flutter/material.dart';
import 'authentication/login_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(const Duration(milliseconds: 2500), (){
      Navigator.of(context).pop();
      Navigator.of(context).push(MaterialPageRoute(builder: (_) => const LoginScreen()));
      Navigator.popAndPushNamed(context, 'login');
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          color: const Color(0xff64b5f6),
          padding: const EdgeInsets.all(16),
          child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset('assets/images/splashscreen.png',width:  MediaQuery.of(context).size.width,
                    height:  MediaQuery.of(context).size.height * 0.4,),
                  //const Text("Loading...", style: TextStyle(color: Colors.white, fontSize: 23),),
                ],
              )
          ),
        ),
      ),
    );
  }
}
